package com.hhgabor.recipepuppy.api;


import com.hhgabor.recipepuppy.model.BaseRecipeResponse;

import io.reactivex.Single;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface RecipePuppyApiClient {

    @GET("/api")
    Single<BaseRecipeResponse> getBaseRecipeResponse(@Query("q") String recipeName);
}
