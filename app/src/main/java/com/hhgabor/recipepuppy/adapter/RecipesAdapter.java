package com.hhgabor.recipepuppy.adapter;


import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.hhgabor.recipepuppy.R;
import com.hhgabor.recipepuppy.model.Recipe;

import java.util.ArrayList;
import java.util.List;

public class RecipesAdapter extends RecyclerView.Adapter<RecipesAdapter.ViewHolder> {

    public interface ItemClickListener {
        void onItemClick(Recipe recipe, int position);
    }

    private List<Recipe> items = new ArrayList<>();
    private ItemClickListener itemClickListener;

    public RecipesAdapter(List<Recipe> items) {
        setHasStableIds(true);
        updateItems(items);
    }

    public void updateItems(List<Recipe> items) {
        this.items = items;
        notifyDataSetChanged();
    }

    public void clearItems() {
        updateItems(new ArrayList<>());
    }

    public void setItemClickListener(ItemClickListener listener) {
        itemClickListener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_recipe, parent, false);
        return new ViewHolder(v, itemClickListener);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.bind(items.get(position), position);
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    @Override
    public long getItemId(int position) {
        return position + 1L;
    }

    public Recipe getItem(int position) {
        if (position >= 0 && position < getItemCount()) {
            return items.get(position);
        } else {
            return null;
        }
    }

    public int getPosition(Recipe recipe) {
        return items.indexOf(recipe);
    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        TextView recipeTitle;

        private Recipe recipe;
        private int position;

        ViewHolder(View itemView, ItemClickListener itemClickListener) {
            super(itemView);

            recipeTitle = (TextView) itemView.findViewById(R.id.recipe_title);

            LinearLayout itemContainer = (LinearLayout) itemView.findViewById(R.id.recipe_item_container);
            itemContainer.setOnClickListener(view -> {
                if (itemClickListener != null) {
                    itemClickListener.onItemClick(recipe, position);
                }
            });
        }

        private void bind(Recipe recipe, int position) {
            this.recipe = recipe;
            this.position = position;
            recipeTitle.setText(recipe.getTitle());
        }
    }

}
