package com.hhgabor.recipepuppy;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.widget.Toast;

import com.hhgabor.recipepuppy.adapter.RecipesAdapter;
import com.hhgabor.recipepuppy.api.RecipePuppyApiClient;
import com.hhgabor.recipepuppy.model.Recipe;
import com.jakewharton.rxbinding2.support.v7.widget.RxSearchView;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class SearchActivity extends AppCompatActivity {

    private static final String TAG = SearchActivity.class.getName();
    private static final Integer NUMBER_OF_QUERIED_RECIPES = 20;
    private static final Integer QUERY_DEBOUNCE_MILLIS = 200;

    private SearchView searchView;
    private RecyclerView recipesRecyclerView;
    private RecipesAdapter recipesAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);

        searchView = (SearchView) findViewById(R.id.search_view);
        recipesRecyclerView = (RecyclerView) findViewById(R.id.recipes_recycler_view);

        setRecyclerView();
        setRetrofit();
    }

    private void setRecyclerView() {
        recipesRecyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        recipesAdapter = new RecipesAdapter(new ArrayList<>());
        recipesAdapter.setItemClickListener((recipe, position) ->
                Toast.makeText(SearchActivity.this, "OnItemClick", Toast.LENGTH_SHORT).show());
        recipesRecyclerView.setAdapter(recipesAdapter);
    }

    private void setRetrofit() {
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();

        Retrofit.Builder builder =
                new Retrofit.Builder()
                        .baseUrl(BuildConfig.API_BASE_URL)
                        .addCallAdapterFactory(RxJava2CallAdapterFactory.createWithScheduler(Schedulers.io()))
                        .addConverterFactory(GsonConverterFactory.create());

        Retrofit retrofit = builder.client(httpClient.build()).build();

        RecipePuppyApiClient client = retrofit.create(RecipePuppyApiClient.class);

        RxSearchView.queryTextChanges(searchView)
                .filter(charSequence -> {
                    if (charSequence.length() == 0) {
                        recipesAdapter.clearItems();
                        return false;
                    }
                    return true;
                })
                .debounce(QUERY_DEBOUNCE_MILLIS, TimeUnit.MILLISECONDS)
                .flatMapSingle(charSequence -> client.getBaseRecipeResponse(charSequence.toString()))
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(baseRecipeResponse -> {
                    if (baseRecipeResponse != null && baseRecipeResponse.getRecipeList() != null) {
                        List<Recipe> recipeList = baseRecipeResponse.getRecipeList();
                        recipesAdapter.updateItems(recipeList.subList(0, Math.min(NUMBER_OF_QUERIED_RECIPES, recipeList.size())));
                    }
                }, throwable -> Log.d(TAG, throwable.toString()));
    }
}
