package com.hhgabor.recipepuppy.model;


public class Recipe {

    private String title;

    private String href;

    private String ingredients;

    private String thumbnail;

    public Recipe() {
    }

    public String getTitle() {
        return title;
    }

    public String getHref() {
        return href;
    }

    public String getIngredients() {
        return ingredients;
    }

    public String getThumbnail() {
        return thumbnail;
    }
}
