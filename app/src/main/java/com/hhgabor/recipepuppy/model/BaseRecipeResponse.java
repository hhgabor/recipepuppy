package com.hhgabor.recipepuppy.model;


import com.google.gson.annotations.SerializedName;

import java.util.List;

public class BaseRecipeResponse {

    private String title;

    private String version;

    private String href;

    @SerializedName("results")
    private List<Recipe> recipeList;

    public BaseRecipeResponse() {
    }

    public String getTitle() {
        return title;
    }

    public String getVersion() {
        return version;
    }

    public String getHref() {
        return href;
    }

    public List<Recipe> getRecipeList() {
        return recipeList;
    }
}
